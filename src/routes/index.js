import Vue from 'vue'
import Home from '@/views/Home'
import Privacy from '@/views/Privacy'
import VueRouter from 'vue-router'
import privacyroutes from './privacyIndex'

Vue.use(VueRouter)

const routes = [
  {
    path: '/privacy-policy',
    component: Privacy,
    children: privacyroutes
  },
  { path: '/:pathMatch(.*)*', name: 'Home', component: Home }
]

const router = new VueRouter({
  routes
})

export default router
