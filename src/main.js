import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import router from './routes/index'
import {
  faUserAstronaut,
  faEnvelope,
  faCog,
  faLaptopCode,
  faGem,
  faMicrochip,
  faDatabase,
  faTerminal,
  faChartPie,
  faBolt,
  faAppleAlt,
  faThumbtack,
  faPenFancy
} from '@fortawesome/free-solid-svg-icons'
import {
  faLinkedinIn,
  faGitlab,
  faGithub,
  faNodeJs,
  faReact,
  faLaravel,
  faPython,
  faLinux,
  faJava
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import store from './store'

library.add(
  faUserAstronaut,
  faEnvelope,
  faLinkedinIn,
  faGitlab,
  faGithub,
  faCog,
  faLaptopCode,
  faNodeJs,
  faReact,
  faLaravel,
  faPython,
  faGem,
  faMicrochip,
  faDatabase,
  faTerminal,
  faChartPie,
  faLinux,
  faJava,
  faBolt,
  faAppleAlt,
  faThumbtack,
  faPenFancy
)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
