const functions = require("firebase-functions");
const admin = require("firebase-admin");
const nodemailer = require("nodemailer");
const cors = require("cors")({ origin: true });
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: gmailEmail,
    pass: gmailPassword
  }
});

admin.initializeApp(functions.config().firebase);

let db = admin.firestore();

exports.submit = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    if (req.method !== "POST") {
      return;
    }

    const mailOptions = {
      from: req.body.email,
      replyTo: req.body.email,
      to: gmailEmail,
      subject: `from my website ${req.body.email}`,
      text: req.body.message,
      html: `<p>${req.body.message}`
    };

    const notifyOptions = {
      from: req.body.email,
      to: "will@loblollysoftware.com",
      subject: "new email to help@loblollysoftware.com",
      text: "new email received at help email for website"
    };

    const st = req.body.email.toString() + Date.now().toString();

    try {
      await mailTransport.sendMail(mailOptions).then(res => {
        db.collection("inquiries")
          .doc(st)
          .set({
            email: req.body.email,
            name: req.body.name,
            message: req.body.message
          });
        return console.log("ok");
      });
      await mailTransport.sendMail(notifyOptions);
      await res.status(200).send({ isEmailSend: true });
    } catch (error) {
      console.error("error sending email:", error);
    }
  });
});
